# /api/tests.py

from django.test import TestCase
from .models import Usuario


class ModelTestCase(TestCase):
    """This class defines the test suite for the bucketlist model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.usuario = Usuario(name="josjaso kk")

    def test_model_can_create_a_bucketlist(self):
        """Test the bucketlist model can create a bucketlist."""
        old_count = Usuario.objects.count()
        self.usuario.save()
        new_count = Usuario.objects.count()
        self.assertNotEqual(old_count, new_count)
