from django.contrib.auth.models import AbstractUser
from django.db import models


class Usuario(AbstractUser):
    class Meta:
        app_label = 'api'

    adicional = models.CharField(
        max_length=2,
        default='1'
    )


class Receta(models.Model):
    class Meta:
        app_label = 'api'

    usuario = models.OneToOneField(
        Usuario, on_delete=models.CASCADE)

    json = models.CharField(max_length=1000, default='')
