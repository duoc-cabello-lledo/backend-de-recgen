# api/urls.py

from django.conf.urls import url, include
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateView, CreateRecetaView, login

urlpatterns = {
    url(r'^usuarios/$', CreateView.as_view(), name="create"),
    url(r'^recetas/$', CreateRecetaView.as_view(), name="create"),
    path('login/', login)
}

urlpatterns = format_suffix_patterns(urlpatterns)
