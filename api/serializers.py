# api/serializers.py

from rest_framework import serializers
from .models import Usuario, Receta


class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('id', 'username', 'email', 'password')

    def create(self, validated_data):
        user = super(UsuarioSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class RecetaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receta
        fields = ('usuario', 'json')
